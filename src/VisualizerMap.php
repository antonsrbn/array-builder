<?php
/** @noinspection PhpUnused */

/** @noinspection PhpUndefinedClassInspection */

namespace Toxaw\ArrayBuilder;

use Toxaw\ArrayBuilder\ArrayBuilderEntities\Element;

class VisualizerMap
{
    private static $paddingLeft = 20;

    /**
     * @param \Toxaw\ArrayBuilder\ArrayBuilderEntities\Element $element
     */
    public static function visuald(Element $element): void
    {
        self::visual($element);
        die();
    }

    /**
     * @param \Toxaw\ArrayBuilder\ArrayBuilderEntities\Element $element
     */
    public static function visual(Element $element): void
    {
        self::open(true);
        self::nameRoot(self::transName($element));
        self::recursive($element);
        self::nameRoot(self::transNameClosed($element));
         self::closed();
    }

    /**
     * @param \Toxaw\ArrayBuilder\ArrayBuilderEntities\Element $element
     */
    private static function recursive(Element $element): void
    {
        self::open();
        foreach ($element->getChildes() as $child) {
            if ($child->getMerged() === null) {
                self::name(self::transName($child));
            } else {
                self::mergedName(self::transName($child));
            }
            self::recursive($child);
            if ($child->getMerged() === null) {
                self::name(self::transNameClosed($child));
            } else {
                self::mergedName(self::transNameClosed($child));
            }
        }
        self::closed();
    }

    /**
     * @param $name
     */
    private static function nameRoot($name): void
    {
        ?>
        <div style="color:red">
            <?=$name?>
        <?php
    }

    /**
     * @param $name
     */
    private static function name($name): void
    {
        ?>
        <div style="color:blue">
            <?=$name?>
        <?php
    }

    /**
     * @param $name
     */
    private static function mergedName($name): void
    {
        ?>
        <div style="color:green">
            <?=$name?>
        <?php
    }

    /**
     * @param $name
     * @return string
     */
    private static function marginLeft($name): string
    {
        return '<div style="margin-left:' . self::$paddingLeft . 'px">' . $name . '</div>';
    }

    /**
     * @param $name
     * @return string
     */
    private static function marginLeftOpen($name): string
    {
        return '<div style="margin-left:' . self::$paddingLeft . 'px">' . $name;
    }

    /**
    * @param bool $isRoot
    */
    private static function open(bool $isRoot = false): void
    {
        ?>
        <div style="
        margin-left: <?=!$isRoot ? self::$paddingLeft : '0'?>px;
        background-color: rgba(0, 0, 0, .1);
        ">
        <?php
    }

    /**
     *
     */
    private static function closed(): void
    {
        ?>
        </div>
        <?php
    }

    /**
     * @param \Toxaw\ArrayBuilder\ArrayBuilderEntities\Element $element
     * @return string
     */
    private static function transName(Element $element): string
    {
        $isOne    = $element->isOne();
        $name     = $element->getName();
        $isMerged = $element->getMerged() !== null;
        if ($element->getParent() === null) {
            $name = '-> ' . $name;

        } else {
            $name = $isMerged ? '(' . $name . ')' : '[' . $name . '] =>';
        }
        $name .= '<br>';
        $more = '<br>' . self::marginLeft('...');
        return $isOne ? ($name . ($isMerged ? '...' : (self::marginLeftOpen('{') . $more))) :
            ($name . ($isMerged ? '...' : (self::marginLeftOpen('[') . '<br>' . self::marginLeftOpen('{') . $more)));
    }

    /**
     * @param \Toxaw\ArrayBuilder\ArrayBuilderEntities\Element $element
     * @return string
     */
    private static function transNameClosed(Element $element): string
    {
        $isOne    = $element->isOne();
        $isMerged = $element->getMerged() !== null;
        $more = '<br>' . self::marginLeft('...');
        return ($isOne ? ('' . ($isMerged ? '' : '}</div>')) :
            ('' . ($isMerged ? '' : '}</div>,<br>{' . $more . '</div>]'))) . '</div></div>';
    }
}


