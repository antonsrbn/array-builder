<?php
/** @noinspection PhpUnused */

/** @noinspection PhpUndefinedClassInspection */

namespace Toxaw\ArrayBuilder\ArrayBuilderEntities;

use RuntimeException;

class Container
{
    /**
     * @var \Toxaw\ArrayBuilder\ArrayBuilderEntities\Element
     */
    protected $element;
    /**
     * @var array
     */
    protected $array;
    /**
     * @var array
     */
    protected $nodes;
    /**
     * @var string|null
     */
    protected $name;
    /**
     * @var boolean
     */
    protected $saveKeys;
    /**
     * @var array
     */
    protected $unsetFields;
    /**
     * @var array
     */
    protected $callables;

    /**
     * Node constructor.
     * @param \Toxaw\ArrayBuilder\ArrayBuilderEntities\Element $element
     */
    public function __construct(Element $element)
    {
        $this->element     = $element;
        $this->nodes       = [];
        $this->name        = null;
        $this->saveKeys    = false;
        $this->unsetFields = [];
        $this->callables   = [];
        $this->name        = $this->element->getName();
    }

    /**
     * @return \Toxaw\ArrayBuilder\ArrayBuilderEntities\Element
     */
    public function getElement(): Element
    {
        return $this->element;
    }

    /**
     * @param array $array
     * @return \Toxaw\ArrayBuilder\ArrayBuilderEntities\Container
     */
    public function fill(array $array): Container
    {
        $this->array = $array;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getArray(): ?array
    {
        return $this->array;
    }

    /**
     * @param string $name
     * @return \Toxaw\ArrayBuilder\ArrayBuilderEntities\Container
     */
    public function setName(string $name): Container
    {
        if ($this->element->getParent() !== null) {
            $this->name = $name;
            return $this;
        }
        $rootElementName = $this->element->getName();
        throw new RuntimeException("Корневой элемент c именем '$rootElementName' не может именоваться!");
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @return \Toxaw\ArrayBuilder\ArrayBuilderEntities\Node
     */
    public function addNode(): Node
    {
        if ($this->element->getParent() !== null) {
            $node          = new Node($this->element, $this);
            $this->nodes[] = $node;
            return $node;
        }
        $rootElementName = $this->element->getName();
        throw new RuntimeException("Корневой элемент c именем '$rootElementName' не может иметь привязок!");
    }

    /**
     * @return array
     */
    public function getNodes(): array
    {
        return $this->nodes;
    }

    /**
     * @return bool
     */
    public function isSaveKeys(): bool
    {
        return $this->saveKeys;
    }

    /**
     * @return \Toxaw\ArrayBuilder\ArrayBuilderEntities\Container
     */
    public function setSaveKeys(): Container
    {
        $this->saveKeys = true;
        return $this;
    }

    /**
     * @param string $name
     * @return \Toxaw\ArrayBuilder\ArrayBuilderEntities\Container
     */
    public function addUnsetField(string $name): Container
    {
        $this->unsetFields[] = $name;
        return $this;
    }

    /**
     * @return array
     */
    public function getUnsetFields(): array
    {
        return $this->unsetFields;
    }

    /**
     * @param callable $callable
     * @return $this
     */
    public function addCallable(callable $callable): Container
    {
        $this->callables[] = $callable;
        return $this;
    }

    /**
     * @return array
     */
    public function getCallables(): array
    {
        return $this->callables;
    }
}


