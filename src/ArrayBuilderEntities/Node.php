<?php
/** @noinspection PhpUnused */

/** @noinspection PhpUndefinedClassInspection */

namespace Toxaw\ArrayBuilder\ArrayBuilderEntities;

use RuntimeException;

class Node
{
    /**
     * @var \Toxaw\ArrayBuilder\ArrayBuilderEntities\Element
     */
    protected $element;
    /**
     * @var \Toxaw\ArrayBuilder\ArrayBuilderEntities\Container
     */
    protected $container;
    /**
     * @var \Toxaw\ArrayBuilder\ArrayBuilderEntities\Element
     */
    protected $parent;
    /**
     * @var string
     */
    protected $foreign;
    /**
     * @var boolean
     */
    protected $unsetForeign;
    /**
     * @var string
     */
    protected $primary;
    /**
     * @var boolean
     */
    protected $unsetPrimary;
    /**
     * @var \Toxaw\ArrayBuilder\ArrayBuilderEntities\Condition
     */
    protected $condition;

    /**
     * Node constructor.
     * @param \Toxaw\ArrayBuilder\ArrayBuilderEntities\Element   $element
     * @param \Toxaw\ArrayBuilder\ArrayBuilderEntities\Container $container
     */
    public function __construct(Element $element, Container $container)
    {
        $this->element      = $element;
        $this->container    = $container;
        $this->unsetForeign = false;
        $this->unsetPrimary = false;
        $this->condition    = new Condition($this);
    }

    /**
     * @return \Toxaw\ArrayBuilder\ArrayBuilderEntities\Element
     */
    public function getElement(): Element
    {
        return $this->element;
    }

    /**
     * @param string $name
     * @return \Toxaw\ArrayBuilder\ArrayBuilderEntities\Node
     */
    public function setForeign(string $name): Node
    {
        $this->foreign = $name;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getForeign(): ?string
    {
        return $this->foreign;
    }

    /**
     * @return \Toxaw\ArrayBuilder\ArrayBuilderEntities\Node
     */
    public function unsetForeign(): Node
    {
        $this->unsetForeign = true;
        return $this;
    }

    /**
     * @return bool
     */
    public function isUnsetForeign(): bool
    {
        return $this->unsetForeign;
    }

    /**
     * @param string $name
     * @return \Toxaw\ArrayBuilder\ArrayBuilderEntities\Node
     */
    public function setParent(string $name): Node
    {
        $this->parent = $this->findParent($name);
        return $this;
    }

    /**
     * @return \Toxaw\ArrayBuilder\ArrayBuilderEntities\Element|null
     */
    public function getParent(): ?Element
    {
        return $this->parent;
    }

    /**
     * @param string $name
     * @return \Toxaw\ArrayBuilder\ArrayBuilderEntities\Element
     */
    protected function findParent(string $name): Element
    {
        $parents = [];
        $parent  = $this->element->getParent();
        while ($parent) {
            if ($parent->getName() === $name) {
                return $parent;
            }
            $parents[] = $parent->getName();
            $parent    = $parent->getParent();
        }
        $curName = $this->element->getName();
        throw new RuntimeException("Родительский элемент c именем '$name' не найден! "
            . implode(' < ', array_reverse($parents)) . " < '$curName' ");
    }


    /**
     * @param string $name
     * @return \Toxaw\ArrayBuilder\ArrayBuilderEntities\Node
     */
    public function setPrimary(string $name): Node
    {
        $this->primary = $name;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPrimary(): ?string
    {
        return $this->primary;
    }

    /**
     * @return \Toxaw\ArrayBuilder\ArrayBuilderEntities\Node
     */
    public function unsetPrimary(): Node
    {
        $this->unsetPrimary = true;
        return $this;
    }

    /**
     * @return bool
     */
    public function isUnsetPrimary(): bool
    {
        return $this->unsetPrimary;
    }

    /**
     * @return \Toxaw\ArrayBuilder\ArrayBuilderEntities\Condition
     */
    public function condition(): Condition
    {
        return $this->condition;
    }

    /**
     * @return \Toxaw\ArrayBuilder\ArrayBuilderEntities\Container
     * @noinspection NotOptimalIfConditionsInspection
     */
    public function tie(): Container
    {
        if ($this->condition->isAll() || ($this->foreign && $this->parent && $this->primary)) {
            return $this->container;
        }
        $elementName = $this->element->getName();
        throw new RuntimeException("Одна из привязки у контейнера
        элемента с именем '$elementName' не доконца задана!");
    }
}


