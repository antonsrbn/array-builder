<?php
/** @noinspection PhpUnused */

/** @noinspection PhpUndefinedClassInspection */

namespace Toxaw\ArrayBuilder\ArrayBuilderEntities;

class Condition
{
    /**
     * @var callable
     */
    protected $operation;
    /**
     * @var \Toxaw\ArrayBuilder\ArrayBuilderEntities\Node
     */
    protected $node;
    /**
     * @var bool
     */
    protected $all;

    /**
     * Condition constructor.
     * @param \Toxaw\ArrayBuilder\ArrayBuilderEntities\Node $node
     */
    public function __construct(Node $node)
    {
        $this->node = $node;
        $this->all  = false;
        $this->equally();
    }

    /**
     * @param $valueLeft
     * @param $valueRight
     * @return bool
     */
    public function __invoke($valueLeft, $valueRight): bool
    {
        return ($this->operation)($valueLeft, $valueRight) === true;
    }

    /**
     * @return \Toxaw\ArrayBuilder\ArrayBuilderEntities\Node
     * @noinspection PhpUnusedParameterInspection
     * @noinspection NullPointerExceptionInspection
     */
    public function all(): Node
    {
        $this->all = true;
        $this->node->setParent($this->node->getElement()->getParent()->getName());
        $this->operation = static function ($valueLeft, $valueRight) {
            return true;
        };
        return $this->node;
    }

    /**
     * @return bool
     */
    public function isAll(): bool
    {
        return $this->all;
    }

    /**
     * @return \Toxaw\ArrayBuilder\ArrayBuilderEntities\Node
     */
    public function equally(): Node
    {
        $this->operation = static function ($valueLeft, $valueRight) {
            return $valueLeft === $valueRight;
        };
        return $this->node;
    }

    /**
     * @param bool $strict
     * @return \Toxaw\ArrayBuilder\ArrayBuilderEntities\Node
     */
    public function primaryInForeign(bool $strict = true): Node
    {
        $this->operation = static function ($valueLeft, $valueRight) use ($strict) {
            return in_array($valueRight, $valueLeft, $strict);
        };
        return $this->node;

    }

    /**
     * @param bool $strict
     * @return \Toxaw\ArrayBuilder\ArrayBuilderEntities\Node
     */
    public function foreignInPrimary(bool $strict = true): Node
    {
        $this->operation = static function ($valueLeft, $valueRight) use ($strict) {
            return in_array($valueLeft, $valueRight, $strict);
        };
        return $this->node;
    }

    /**
     * @param callable $operation
     * @return \Toxaw\ArrayBuilder\ArrayBuilderEntities\Node
     */
    public function callable(callable $operation): Node
    {
        $this->operation = $operation;
        return $this->node;
    }
}


