<?php
/** @noinspection PhpUnused */

/** @noinspection PhpUndefinedClassInspection */

namespace Toxaw\ArrayBuilder\ArrayBuilderEntities;

use RuntimeException;

class Element
{
    /**
     * @var string
     */
    protected $name;
    /**
     * @var \Toxaw\ArrayBuilder\ArrayBuilderEntities\Element|null
     */
    protected $parentElement;
    /**
     * @var array
     */
    protected $childElements;
    /**
     * @var \Toxaw\ArrayBuilder\ArrayBuilderEntities\Merged|null
     */
    protected $merged;
    /**
     * @var bool
     */
    protected $one;
    /**
     * @var bool
     */
    protected $notEmpty;

    /**
     * Element constructor.
     * @param string                                                 $name
     * @param \Toxaw\ArrayBuilder\ArrayBuilderEntities\Element|null $parentElement
     */
    public function __construct(string $name, Element $parentElement = null)
    {
        $this->parentElement = $parentElement;
        $this->validateName($name);
        $this->name          = $name;
        $this->childElements = [];
        $this->merged        = null;
        $this->one           = false;
        $this->notEmpty      = false;
    }

    /**
     * @param string $name
     * @return \Toxaw\ArrayBuilder\ArrayBuilderEntities\Element
     */
    public function addChild(string $name): Element
    {
        if ($this->merged) {
            $this->exceptionShowPath("При установленной опции inMerged наследник '$name' не может существовать!", $name);
        }
        if ($this->childElements[$name]) {
            $this->exceptionShowPath("Имя '$name' между childes должно быть уникальным!", $name);
        }
        $child                      = new Element($name, $this);
        $this->childElements[$name] = $child;
        return $child;
    }

    public function getChild(string $name): Element
    {
        if (!$this->childElements[$name]) {
            $this->exceptionShowPath("Child с именем '$name' не найден!", $name);
        }
        return $this->childElements[$name];
    }

    /**
     * @return array
     */
    public function getChildes(): array
    {
        return $this->childElements;
    }

    /**
     * @param string $name
     */
    protected function validateName(string $name): void
    {
        $parents = [];
        $parent  = $this->getParent();
        while ($parent) {
            if ($parent->getName() === $name) {
                $parents[] = '[' . $parent->getName() . ']';
                throw new RuntimeException("Имя '$name' должно не совпадать с именами родителей! "
                    . implode(' < ', array_reverse($parents)) . " < '$name' ");
            }
            $parents[] = $parent->getName();
            $parent    = $parent->getParent();
        }
    }


    /**
     * @param string      $message
     * @param string|null $name
     */
    protected function exceptionShowPath(string $message, string $name = null): void
    {
        $parents   = [];
        $parents[] = $name === null ? "'{$this->getName()}'" : $this->getName();
        $parent    = $this->getParent();
        while ($parent) {
            $parents[] = $parent->getName();
            $parent    = $parent->getParent();
        }
        throw new RuntimeException($message . ' '
            . implode(' < ', array_reverse($parents)) . ($name !== null ? " < '$name' " : ''));
    }

    /**
     * @return \Toxaw\ArrayBuilder\ArrayBuilderEntities\Element|null
     */
    public function getParent(): ?Element
    {
        return $this->parentElement;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return \Toxaw\ArrayBuilder\ArrayBuilderEntities\Merged
     */
    public function inMerged(): Merged
    {
        if ($this->childElements) {
            $this->exceptionShowPath("Элемент '{$this->name}' уже имеет наследников!");
        }
        if ($this->merged !== null) {
            $this->exceptionShowPath("Элемент '{$this->name}' уже имеет опцию inMerged!");
        }
        $this->merged = new Merged();
        return $this->merged;
    }

    /**
     * @return \Toxaw\ArrayBuilder\ArrayBuilderEntities\Merged|null
     */
    public function getMerged(): ?Merged
    {
        return $this->merged;
    }

    /**
     * @return \Toxaw\ArrayBuilder\ArrayBuilderEntities\Element
     */
    public function one(): Element
    {
        $this->one = true;
        return $this;
    }

    /**
     * @return bool
     */
    public function isOne(): bool
    {
        return $this->one;
    }

    /**
     * @return \Toxaw\ArrayBuilder\ArrayBuilderEntities\Element
     */
    public function notEmpty(): Element
    {
        $this->notEmpty = true;
        return $this;
    }

    /**
     * @return bool
     */
    public function isNotEmpty(): bool
    {
        return $this->notEmpty;
    }
}


