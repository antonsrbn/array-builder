<?php
/** @noinspection PhpUnused */

/** @noinspection PhpUndefinedClassInspection */

namespace Toxaw\ArrayBuilder\ArrayBuilderEntities;

class Merged
{
    /**
     * @var boolean;
     */
    protected $setGroupField;
    /**
     * @var boolean;
     */
    protected $setGroupFieldIfMore;

    /**
     * Merged constructor.
     */
    public function __construct()
    {
        $this->setGroupField       = false;
        $this->setGroupFieldIfMore = false;
    }

    /**
     * @return bool
     */
    public function isSetGroupField(): bool
    {
        return $this->setGroupField;
    }

    /**
     *
     */
    public function setGroupField(): void
    {
        $this->setGroupField = true;
    }

    /**
     * @return bool
     */
    public function isSetGroupFieldIfMore(): bool
    {
        return $this->setGroupFieldIfMore;
    }

    /**
     *
     */
    public function setGroupFieldIfMore(): void
    {
        $this->setGroupFieldIfMore = true;
    }
}


