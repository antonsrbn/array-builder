<?php
/** @noinspection PhpUnused */

/** @noinspection PhpUndefinedClassInspection */

namespace Toxaw\ArrayBuilder;

use Toxaw\ArrayBuilder\ArrayBuilderEntities\Element;
use Toxaw\ArrayBuilder\ArrayBuilderEntities\Container;
use Toxaw\ArrayBuilder\ArrayBuilderEntities\Node;

use RuntimeException;

class Compiler
{
    /**
     * @var \Toxaw\ArrayBuilder\ArrayBuilderEntities\Element
     */
    protected $map;
    /**
     * @var array
     */
    protected $containers;

    /**
     * Compiler constructor.
     * @param \Toxaw\ArrayBuilder\ArrayBuilderEntities\Element $map
     */
    public function __construct(Element $map)
    {
        $this->map        = $map;
        $this->containers = [];
    }

    /**
     * @param bool $verification
     * @return array
     */
    public function compile(bool $verification = true): array
    {
        if ($verification) {
            $this->checkIdentifiedElements();
            $this->checkFullnessContainers();
        }

        return $this->compileRecursive($this->containers[$this->map->getName()]);
    }

    /**
     * @method This method is core for compiler
     * TODO: Please, rewrite and decompose this method, because it's govnokod
     * @param \Toxaw\ArrayBuilder\ArrayBuilderEntities\Container $container
     * @param array                                              $elements
     * @return array
     * @noinspection PhpUndefinedMethodInspection
     * @noinspection SlowArrayOperationsInLoopInspection
     */
    protected function compileRecursive(Container $container, array $elements = []): array
    {
        $containerArray = [];
        if ($container->getElement()->isOne()) {
            $containerArray[0] = $container->getArray();
        } else {
            $containerArray = $container->getArray();
        }
        $array           = [];
        $nodes           = $container->getNodes();
        $isRootContainer = $container->getElement()->getParent() === null;
        foreach ($containerArray as $key => $item) {
            $itemOrigin       = $item;
            $successNodesKeys = [];
            $isNodes          = false;
            foreach ($nodes as $nodeKey => $node) {
                $isNodes = true;
                if (!($node->condition())($itemOrigin[$node->getForeign()], $elements['merged'][$node->getParent()->getName()][$node->getPrimary()])) {
                    $successNodesKeys = [];
                    break;
                }
                $successNodesKeys[] = $nodeKey;
            }
            $isCallableTrue = true;
            $isCallables    = false;
            if (!$isNodes || ($isNodes && $successNodesKeys)) {
                foreach ($container->getCallables() as $callable) {
                    $isCallables = true;
                    if ($callable($item, $elements) !== true) {
                        $isCallableTrue = false;
                        break;
                    }
                }
            }
            // $isNodes && $isCallables && $successNodesKeys && $isCallableTrue - если есть связи и калбэки и все они прошли
            // $isNodes && !$isCallables && $successNodesKeys - если есть связи и они успешные и нет калбэков
            // !$isNodes && $isCallables && $isCallableTrue - если есть калбэки и они успешные и нет связей
            // $isRootContainer && !$isCallables - если руты и без калбэков
            if (($isNodes && $isCallables && $successNodesKeys && $isCallableTrue)
                || ($isNodes && !$isCallables && $successNodesKeys)
                || (!$isNodes && $isCallables && $isCallableTrue)
                || ($isRootContainer && !$isCallables)) {
                foreach ($nodes as $nodeKey => $node) {
                    if (!in_array($nodeKey, $successNodesKeys, true)) {
                        continue;
                    }
                    if ($node->isUnsetForeign() && array_key_exists($node->getForeign(), $item)) {
                        unset($item[$node->getForeign()]);
                    }
                    if ($node->isUnsetPrimary() && array_key_exists($node->getPrimary(), $elements['modified'][$node->getParent()->getName()])) {
                        unset($elements['modified'][$node->getParent()->getName()][$node->getPrimary()]);
                    }
                }
                foreach ($container->getElement()->getChildes() as $child) {
                    $childContainer                                            = $this->containers[$child->getName()];
                    $elements['modified'][$container->getElement()->getName()] = &$item;
                    $elements['merged'][$container->getElement()->getName()]   = array_merge($item, $itemOrigin);
                    $recursiveResult                                           = $this->compileRecursive($childContainer, $elements);
                    $merged                                                    = $child->getMerged();
                    if ($merged === null) {
                        $item[$childContainer->getName()] = $recursiveResult;
                    } elseif ($recursiveResult) {
                        $one = null;
                        if ($merged->isSetGroupField()) {
                            $one = [];
                            foreach ($recursiveResult as $recursiveResultItem) {
                                $one = array_merge_recursive($one, $recursiveResultItem);
                            }
                            if (count($recursiveResult) === 1) {
                                foreach ($one as &$oneItem) {
                                    $oneItem = [$oneItem];
                                }
                                unset($oneItem);
                            }
                        } elseif ($merged->isSetGroupFieldIfMore()) {
                            foreach ($recursiveResult as $recursiveResultItem) {
                                $one = array_merge_recursive($one, $recursiveResultItem);
                            }

                        }
                        if (!$one) {
                            $one = array_shift($recursiveResult);
                        }
                        $item = array_merge($item, $one);
                    }
                }
                foreach ($container->getUnsetFields() as $unsetField) {
                    if (array_key_exists($unsetField, $item)) {
                        unset($item[$unsetField]);
                    }
                }
                if ($container->isSaveKeys()) {
                    $array[$key] = $item;
                } else {
                    $array[] = $item;
                }
            }
        }
        if ($container->getElement()->isOne()) {
            return $array[0];
        }
        return $array;
    }

    /**
     *
     */
    protected function checkIdentifiedElements(): void
    {
        $elementNames = [];
        $this->getRecursiveNameElements($this->map, $elementNames);
        foreach ($elementNames as $elementName) {
            if (!$this->containers[$elementName]) {
                throw new RuntimeException("Элемент c именем '$elementName' не имеет контейнера!");
            }
        }
    }

    /**
     *
     */
    protected function checkFullnessContainers(): void
    {
        foreach ($this->containers as $elementName => $container) {
            if ($container->getArray() === null) {
                throw new RuntimeException("Элемент c именем '$elementName' не имеет массива контейнера!");
            }
            $this->checkUniformityArray($container->getArray(), $container);

            if ($container->getElement()->getParent()) {
                if (!$container->getName()) {
                    throw new RuntimeException("Элемент c именем '$elementName' не имеет имени контейнера!");
                }
                if (!$container->getNodes() && !$container->getCallables()) {
                    throw new RuntimeException("Элемент c именем '$elementName' не имеет привязок контейнера!");
                }
                foreach ($container->getNodes() as $node) {
                    $this->checkVerificationNode($node, $container);
                }
            }
        }
    }

    /**
     * @param array                                              $array
     * @param \Toxaw\ArrayBuilder\ArrayBuilderEntities\Container $container
     */
    protected function checkUniformityArray(array $array, Container $container): void
    {
        if (!$array || $container->getElement()->isOne()) {
            return;
        }
        $elementName = $container->getElement()->getName();
        $fields      = array_keys(array_shift($array));
        foreach ($array as $key => $item) {
            $fieldsItem = array_keys($item);
            if ($fields !== $fieldsItem) {
                throw new RuntimeException("Элемент c именем '$elementName' имеет неоднородный елемент [$key] в массиве контейнера!");
            }
        }
    }

    /**
     * @param \Toxaw\ArrayBuilder\ArrayBuilderEntities\Node      $node
     * @param \Toxaw\ArrayBuilder\ArrayBuilderEntities\Container $container
     */
    protected function checkVerificationNode(Node $node, Container $container): void
    {
        $array       = $container->getArray();
        $elementName = $container->getElement()->getName();
        if ($node->condition()->isAll()) {
            return;
        }
        if (!$node->getForeign()) {
            throw new RuntimeException("Внешний ключ одной из привязки у контейнера элемента
            с именем '$elementName' не доконца задан!");
        }
        $isForeign = false;
        $arrayItem = [];
        if ($array) {
            if ($container->getElement()->isOne()) {
                $array = [$array];
            }
            $arrayItem = array_keys(array_shift($array));
            if (in_array($node->getForeign(), $arrayItem, true)) {
                $isForeign = true;
            }
        }
        if (!$isForeign && $container->getElement()->isNotEmpty()) {
            throw new RuntimeException("Внешний ключ '{$node->getForeign()}' одной из привязки
            у контейнера элемента с именем '$elementName' не найден!
            Доступные ключи: '" . implode("', '", $arrayItem) . "'.");
        }
        if (!$node->getParent()) {
            throw new RuntimeException("Родительский элемент одной из привязки у
            контейнера элемента с именем '$elementName' не доконца задан!");
        }
        if (!$node->getPrimary()) {
            throw new RuntimeException("Первичный ключ одной из привязки у контейнера
            элемента с именем '$elementName' не доконца задан!");
        }
        $parentArray = $this->containers[$node->getParent()->getName()]->getArray();
        if ($node->getParent()->isOne()) {
            $parentArray = [$parentArray];
        }
        $isPrimary = false;
        $arrayItem = [];
        if ($parentArray) {
            if ($node->getParent()->isOne()) {
                $array = [$array];
            }
            $arrayItem = array_keys(array_shift($parentArray));
            if (in_array($node->getPrimary(), $arrayItem, true)) {
                $isPrimary = true;
            }
        }
        if (!$isPrimary && $node->getParent()->isNotEmpty()) {
            throw new RuntimeException("Первичный ключ '{$node->getPrimary()}' родительского
            элемента '{$node->getParent()->getName()}' одной из привязки у контейнера
            элемента с именем '$elementName' не найден! Доступные ключи: '" . implode("', '", $arrayItem) . "'.");
        }

    }

    /**
     * @param \Toxaw\ArrayBuilder\ArrayBuilderEntities\Element $element
     * @param array                                            $elementNames
     */
    protected function getRecursiveNameElements(Element $element, array &$elementNames): void
    {
        $elementNames[] = $element->getName();
        foreach ($element->getChildes() as $child) {
            $this->getRecursiveNameElements($child, $elementNames);
        }
    }

    /**
     * @param string $name
     * @return \Toxaw\ArrayBuilder\ArrayBuilderEntities\Container
     */
    public function element(string $name): Container
    {
        if ($this->containers[$name]) {
            throw new RuntimeException("Элемент c именем '$name' уже задан!");
        }
        $element                 = $this->findElement($name);
        $container               = new Container($element);
        $this->containers[$name] = $container;
        return $container;
    }

    /**
     * @param string $name
     * @return \Toxaw\ArrayBuilder\ArrayBuilderEntities\Element
     */
    protected function findElement(string $name): Element
    {
        $element = $this->findRecursive($this->map, $name);
        if ($element instanceof Element) {
            return $element;
        }
        throw new RuntimeException("Элемент c именем '$name' не найден!");
    }

    /**
     * @param \Toxaw\ArrayBuilder\ArrayBuilderEntities\Element $element
     * @param string                                           $name
     * @return \Toxaw\ArrayBuilder\ArrayBuilderEntities\Element|null
     */
    protected function findRecursive(Element $element, string $name): ?Element
    {
        if ($element->getName() === $name) {
            return $element;
        }
        foreach ($element->getChildes() as $child) {
            $findElement = $this->findRecursive($child, $name);
            if ($findElement) {
                return $findElement;
            }
        }
        return null;
    }
}


